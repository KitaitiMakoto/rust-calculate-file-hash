use std::collections::hash_map::DefaultHasher;
use std::fs::File;
use std::hash::Hasher;
use std::io::{BufReader, Read};
use std::env::args;

fn main() {
    let file_path = args().into_iter().nth(1).expect("Specify file to calculate hash");
    println!("hash for {}: {}", &file_path, calculate_file_hash(&file_path));
}

fn calculate_file_hash(file_path: &str) -> u64 {
    let file = File::open(file_path).expect("file path");
    let mut reader = BufReader::new(file);
    let mut hasher = DefaultHasher::new();
    let mut buffer = [0; 1024];

    while let Ok(n) = reader.read(&mut buffer) {
        hasher.write(&buffer);

        if n == 0 {
            break;
        }
    }

    hasher.finish()
}
